/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.kuntapong.sqliteproject;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author ASUS
 */
public class InsertUser {
    public static void main(String[] args) {
        Connection conn = null;
        Statement stmt = null;

        try {
            Class.forName("org.sqlite.JDBC");
            conn = DriverManager.getConnection("jdbc:sqlite:user.db");
            conn.setAutoCommit(false);
            stmt = conn.createStatement();
            String sql = "INSERT INTO user (username,password) "
                    + "VALUES ('Paul', '123456Paul');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (username,password) "
                    + "VALUES ('Allen','Allen1112');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (username,password) "
                    + "VALUES ('Teddy', 'Teddyteddy');";
            stmt.executeUpdate(sql);

            sql = "INSERT INTO user (username,password) "
                    + "VALUES ('Mark', '6969mark');";
            stmt.executeUpdate(sql);
            stmt.close();
            conn.commit();
            conn.close();
        } catch (ClassNotFoundException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        } catch (SQLException ex) {
            Logger.getLogger(InsertCompany.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
